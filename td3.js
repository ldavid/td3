const d3 = {}

const isIterable = (obj) => obj != null && typeof obj[Symbol.iterator] === 'function'

const fragment = (obj) => {
  let f = document.createDocumentFragment()
  Object.entries(obj).forEach(([key, value]) => f[key] = value)
  return f;
}

const flatIf1NotEmptyIterableChild = arr => arr.length === 1 && isIterable(arr[0]) && arr[0].length > 0 ? arr[0] : arr

const _v = (val,e,i,arr) => val instanceof Function ? val(e.__data__,i,arr) : val

class Selection {
  constructor(groups, parents) {
    this._groups = Array.from(groups)
    this._parents = parents
  }

  select(selector) {
    return new Selection(this._groups.map(g => {
      if (isIterable(g)) return Array.from(g).map(e => e.querySelector(selector))
      return g.querySelector(selector)
    }), this._parents);
  }

  selectAll(selector) {
    return new Selection(flatIf1NotEmptyIterableChild(this._groups.map(g => {
      if (isIterable(g)) return Array.from(g).map(e => e.querySelectorAll(selector))
      return g.querySelectorAll(selector)
    })), flatIf1NotEmptyIterableChild(this._groups));
  }

  data(d/*, id*/) {
    this.call((e,i,arr) => { e.__data__ = d[i] }) // bind data to existing elements

    let j = 0
    this._enter = this._groups.map((g,i) => {
      if (isIterable(g)) {
        if (d instanceof Function) {
          return this._parents[i].__data__.map((v,j) => g[j] && !(g[j] instanceof NodeList) ? null : fragment({__data__: v}))
        }
        return d.map((v,j) => g[j] && !(g[j] instanceof NodeList) ? null : fragment({__data__: v}))
      }
      if (g) return undefined;
      return fragment({__data__: d[j++]})
    })
    this._exit = this._groups.map((v,i) => d[i] && this._groups[i]) // TODO
    return this;
  }

  enter() { return new Selection(this._enter, this._parents); }
  exit() { return new Selection(this._exit, this._parents); }

  append(val) {
    return this.map((n,j,i,nodes) => {
      if (!n) return;
      let e = val instanceof Function ? val(n,j,nodes) : document.createElement(val)
      e.__data__ = n.__data__
      if (n instanceof DocumentFragment) {
        (this._parents[i] ?? this._parents[0]).appendChild(e)
      } else {
        n.appendChild(e)
      }
      return e
    })
  }

  text(val) { return this.call((e,i,arr) => { if (e) e.textContent = _v(val,e,i,arr) }); }
  style(attr, val) { return this.call((e,i,arr) => e.style.setProperty(attr, _v(val,e,i,arr), null)); }
  attr(attr, val) { return this.call((e,i,arr) => e.setAttribute(attr, _v(val,e,i,arr))); }

  map(f/*, ...parameters*/) {
    return new Selection(this._groups.map((g,i,groups) => {
      if (!g) return
      if (isIterable(g)) return Array.from(g).map((n,j,nodes) => f(n,j,i,nodes))
      return f(g,i,i,groups)
    }), this._parents)
  }

  call(f/*, ...parameters*/) {
    this._groups.forEach((g,i,groups) => {
      if (!g) return;
      if (isIterable(g)) {
        g.forEach((n,j,nodes) => {
          f(n,j,nodes) // i as third arg before nodes ?
        })
        return;
      }
      f(g,i,groups)
    })
    return this
  }

  node() {
    const ret = this._groups[0]
    return isIterable(ret) ? ret[0] : ret
  }

  nodes() { return this._groups }

  each(f) { this.call((node) => f(node.__data__)) }
}

d3.select = selector => new Selection([document.querySelector(selector)], [document.documentElement])
d3.selectAll = selector => new Selection([document.querySelectorAll(selector)], [document.documentElement])

export default d3