# td3

Rewrite [d3js](https://d3js.org/) core as tiny as possible to understand how it works.

- select
- selectAll
- data
- enter
- exit
- append
- text
- style
- attr
- map
- call
- node
- nodes
- each